package com.example.masfuadiiskandarsy.scannercleon2;

/**
 * Created by Masfuadi Iskandar Sy on 11/01/2017.
 */

public class setGETClass {
    private String kodeMitra;
    private String namaMitra;
    private String alamatMitra;

    private String tanggal;
    private String status;

    private String jumlah;
    private String kodeMitraR;
    private String namaMitraR;

    public String getKodeMitraR() {
        return kodeMitraR;
    }

    public void setKodeMitraR(String kodeMitraR) {
        this.kodeMitraR = kodeMitraR;
    }

    public String getNamaMitraR() {
        return namaMitraR;
    }

    public void setNamaMitraR(String namaMitraR) {
        this.namaMitraR = namaMitraR;
    }

    private String CNumber;
    private String jenisVoucher;

    public String getKodeMitra() {
        return kodeMitra;
    }

    public void setKodeMitra(String kodeMitra) {
        this.kodeMitra = kodeMitra;
    }

    public String getNamaMitra() {
        return namaMitra;
    }

    public void setNamaMitra(String namaMitra) {
        this.namaMitra = namaMitra;
    }

    public String getAlamatMitra() {
        return alamatMitra;
    }

    public void setAlamatMitra(String alamatMitra) {
        this.alamatMitra = alamatMitra;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCNumber() {
        return CNumber;
    }

    public void setCNumber(String CNumber) {
        this.CNumber = CNumber;
    }

    public String getJenisVoucher() {
        return jenisVoucher;
    }

    public void setJenisVoucher(String jenisVoucher) {
        this.jenisVoucher = jenisVoucher;
    }

    public void setJumlah(String jumlah){
        this.jumlah=jumlah;
    }
    public String getJumlah(){
        return jumlah;
    }

}
