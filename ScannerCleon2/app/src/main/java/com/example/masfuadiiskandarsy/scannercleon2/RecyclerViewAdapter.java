package com.example.masfuadiiskandarsy.scannercleon2;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Masfuadi Iskandar Sy on 26/01/2017.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private Context context;
    private List<Result> results;

    public RecyclerViewAdapter(Context context, List<Result> results) {
        this.context = context;
        this.results = results;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_mitra, parent, false);
        ViewHolder holder = new ViewHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Result result = results.get(position);
        holder.textViewKodeMitra.setText(result.getKodeMitra());
        holder.textViewNamaMitra.setText(result.getNamaMitra());
        holder.textViewAlamatMitra.setText(result.getAlamatMitra());
        holder.textViewNoHp.setText(result.getNoHp());
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.kodeMitra) TextView textViewKodeMitra;
        @BindView(R.id.namaMitra) TextView textViewNamaMitra;
        @BindView(R.id.alamatMitra) TextView textViewAlamatMitra;
        @BindView(R.id.noHpMitra) TextView textViewNoHp;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            String kodeMitra = textViewKodeMitra.getText().toString();
            Intent i = new Intent(context, scanVoucher.class);
            i.putExtra("kodeMitra", kodeMitra);
            context.startActivity(i);
        }
    }
}
