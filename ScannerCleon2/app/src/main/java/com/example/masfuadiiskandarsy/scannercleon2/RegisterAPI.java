package com.example.masfuadiiskandarsy.scannercleon2;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Masfuadi Iskandar Sy on 26/01/2017.
 */

public interface RegisterAPI {
    @FormUrlEncoded
    @POST("simpanBarcode.php")
    Call<Value> daftar(@Field("tanggal") String tanggal,
                       @Field("status") String status,
                       @Field("kodeMitra") String kodeMitra,
                       @Field("CNumber") String CNumber);

    @GET("listMitra.php")
    Call<Value>view();

    @FormUrlEncoded
    @POST("search.php")
    Call<Value> search(@Field("search") String search);

    @GET("listReport.php")
    Call<Value>viewReport();
}
