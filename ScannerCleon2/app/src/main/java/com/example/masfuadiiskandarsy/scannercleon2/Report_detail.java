package com.example.masfuadiiskandarsy.scannercleon2;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

class RdAdapter extends BaseAdapter{
    private Activity activity;
    private ArrayList<setGETClass> data_rd = new ArrayList<setGETClass>();
    private static LayoutInflater inflater = null;

    public RdAdapter(Activity a, ArrayList<setGETClass>d){
        activity =a; data_rd=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    public int getCount(){
        return data_rd.size();
    }
    public Object getItem(int position){
        return data_rd.get(position);
    }
    public long getItemId(int position){
        return position;
    }
    public View getView(int position, View convertView, ViewGroup parent){
        View vi = convertView;
        if (convertView==null)
            vi = inflater.inflate(R.layout.list_rd, null);
        TextView CNumber_rd = (TextView)vi.findViewById(R.id.CNumber_rd);
        TextView jenisVoucher_rd = (TextView)vi.findViewById(R.id.jenisVoucher_rd);

        setGETClass daftar_rd = data_rd.get(position);
        CNumber_rd.setText(daftar_rd.getCNumber());
        jenisVoucher_rd.setText(daftar_rd.getJenisVoucher());

        return vi;
    }
}

public class Report_detail extends AppCompatActivity implements AdapterView.OnItemClickListener{
private TextView txtkodeMitra, txtnamaMitra, txtjumlah, levelTxt;
    private ListView listRd;
    private String url, urlTarik, levelAdmin, CNumb, level2, urlBatal;
    JSONParser jparser = new JSONParser();
    static boolean a = false;
    ArrayList<setGETClass>daftar_rd = new ArrayList<setGETClass>();
    JSONArray daftarRd = null;
    SessionManager sessionManager;

    private LinearLayout loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_detail);
        txtkodeMitra = (TextView)findViewById(R.id.kodeMitraDR);
        txtnamaMitra = (TextView)findViewById(R.id.namaMitraDR);
        txtjumlah = (TextView)findViewById(R.id.jumlahDR);
        levelTxt = (TextView)findViewById(R.id.leveltxt);

        sessionManager = new SessionManager(getApplicationContext());
        sessionManager.checkLogin();
        HashMap<String, String> user = sessionManager.getUserDetails();
        levelAdmin = user.get(SessionManager.KEY_LEV);
        levelTxt.setText(levelAdmin);
        level2 = levelTxt.getText().toString();

        loading = (LinearLayout)findViewById(R.id.loading);

        listRd = (ListView)findViewById(R.id.listRD);
        listRd.setOnItemClickListener(this);

        Intent i = getIntent();
        String kodeM = i.getStringExtra("kodeMitra");
        String namaM = i.getStringExtra("namaMitra");
        String jumlahh = i.getStringExtra("jumlah");

        txtkodeMitra.setText(kodeM);
        txtnamaMitra.setText(namaM);
        txtjumlah.setText(jumlahh);

        koneksi k = new koneksi();
        String isi = k.isi_koneksi();
        url = isi+"detail_report.php?"+"kodeMitra="+txtkodeMitra.getText().toString();

        new tampil().execute();

    }
    @Override
    public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
        if (level2.equals("ADMIN")){
            final CharSequence[] items = {"Tarik", "Batalkan"};
            AlertDialog.Builder builder = new AlertDialog.Builder(Report_detail.this);
            builder.setTitle("Pilih Menu");
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (items[which]=="Tarik"){
                        CNumb = ((TextView)view.findViewById(R.id.CNumber_rd)).getText().toString();
                        koneksi k = new koneksi();
                        String isi = k.isi_koneksi();
                        urlTarik = isi+"tarik.php";
                        new tarik().execute();
                    }
                    else if (items[which]=="Batalkan"){
                        CNumb = ((TextView)view.findViewById(R.id.CNumber_rd)).getText().toString();
                        koneksi k = new koneksi();
                        String isi = k.isi_koneksi();
                        urlBatal = isi+"urlBatal.php";
                        new batal().execute();
                    }
                }
            }).show();
        }else {
            Toast.makeText(this, "Anda tidak dapat mengakses fitur ini", Toast.LENGTH_SHORT).show();
        }
    }
    public class batal extends AsyncTask<String, Void, String>{
        ProgressDialog pDialog;
        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            pDialog = new ProgressDialog(Report_detail.this);
            pDialog.setMessage("Tunggu...");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();

        }
        @Override
        protected String doInBackground(String...sText){
            List<NameValuePair>parameter = new ArrayList<NameValuePair>();
            parameter.add(new BasicNameValuePair("CNumber", CNumb));
            try{
                JSONObject json = jparser.makeHttpRequest(urlBatal, "POST", parameter);
                int success = json.getInt("success");
                if (success==1){
                    return "ok";
                }
                else {
                    return "fail";
                }
            }catch (Exception e){
                e.printStackTrace();
                return "exception caught";
            }
        }
        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            pDialog.dismiss();
            if (result.equalsIgnoreCase("exception caught")){
                Toast.makeText(Report_detail.this, "tidak ada jaringan", Toast.LENGTH_SHORT).show();
            }
            if (result.equalsIgnoreCase("fail")){
                Toast.makeText(Report_detail.this, "gagal", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(Report_detail.this, "Voucher Di Batalkan", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Report_detail.this, MainActivity.class));
                finish();
            }
        }
    }
    public class tarik extends AsyncTask<String, Void, String>{
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            progressDialog = new ProgressDialog(Report_detail.this);
            progressDialog.setMessage("Tunggu...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String...sText){
            List<NameValuePair>parameter = new ArrayList<NameValuePair>();
            parameter.add(new BasicNameValuePair("CNumber", CNumb));
            try{
                JSONObject json = jparser.makeHttpRequest(urlTarik, "POST", parameter);
                int success = json.getInt("success");
                if (success==1){
                    return "ok";
                }
                else {
                    return "fail";
                }
            }catch (Exception e){
                e.printStackTrace();
                return "exception caught";
            }
        }
        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result.equalsIgnoreCase("exception caught")){
                Toast.makeText(Report_detail.this, "jaringan gagal", Toast.LENGTH_SHORT).show();
            }
            if (result.equalsIgnoreCase("fail")){
                Toast.makeText(Report_detail.this, "gagal", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(Report_detail.this, "berhasil menarik voucher", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Report_detail.this, MainActivity.class));
                finish();
            }
        }
    }
    public class tampil extends AsyncTask<String, Void, JSONObject>{
        @Override
        protected void onPreExecute() {
            loading.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }
        @Override
        protected JSONObject doInBackground(String...args){
            List<NameValuePair>parameter = new ArrayList<NameValuePair>();
            JSONObject json = jparser.makeHttpRequest(url, "POST", parameter);
            if (json==null){
                a=false;
            }else
                a=true;
            return json;
        }
        @Override
        protected void onPostExecute(JSONObject json){
            if (a==true){
                try{
                    setGETClass ok = new setGETClass();
                    int success = json.getInt("success");
                    if (success==1){
                        daftarRd = json.getJSONArray("reportDetail");
                        for (int i = 0; i<daftarRd.length();i++){
                            JSONObject c = daftarRd.getJSONObject(i);
                            ok = new setGETClass();
                            ok.setCNumber(c.getString("CNumber"));
                            ok.setJenisVoucher(c.getString("jenisVoucher"));
                            daftar_rd.add(ok);
                            loading.setVisibility(View.GONE);
                            listRd.setAdapter(new RdAdapter(Report_detail.this, daftar_rd));
                        }
                    }
                    else {
                        loading.setVisibility(View.GONE);
                        Log.e("error", "tidak bisa ambil data 0");
                    }
                }catch (Exception e){
                    Toast.makeText(Report_detail.this, "gagal saaat ambil data", Toast.LENGTH_SHORT).show();
                }
            }
            else {
                Toast.makeText(Report_detail.this, "koneksi gagal", Toast.LENGTH_SHORT).show();
            }
        }
    }
}