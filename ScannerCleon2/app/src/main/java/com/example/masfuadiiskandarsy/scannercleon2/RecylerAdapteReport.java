package com.example.masfuadiiskandarsy.scannercleon2;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by sulistiyanto on 07/12/16.
 */

public class RecylerAdapteReport extends RecyclerView.Adapter<RecylerAdapteReport.ViewHolder> {

    private Context context;
    private List<Result> results;

    public RecylerAdapteReport(Context context, List<Result> results) {
        this.context = context;
        this.results = results;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_report, parent, false);
        ViewHolder holder = new ViewHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Result result = results.get(position);
        holder.textViewKodeMitraR.setText(result.getKodeMitra());
        holder.textViewNamaMitraR.setText(result.getNamaMitra());
        holder.textViewJumlahR.setText("Jumlah Voucher Terdistribusi : "+result.getJumlah());
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.kodeMitraR) TextView textViewKodeMitraR;
        @BindView(R.id.namaMitraR) TextView textViewNamaMitraR;
        @BindView(R.id.jumlahR) TextView textViewJumlahR;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            String kodeMitraR = textViewKodeMitraR.getText().toString();
            String namaMitraR = textViewNamaMitraR.getText().toString();
            String jumlahR = textViewJumlahR.getText().toString();

            Intent i = new Intent(context, Report_detail.class);
            i.putExtra("kodeMitra", kodeMitraR);
            i.putExtra("namaMitra", namaMitraR);
            i.putExtra("jumlah", jumlahR);
            context.startActivity(i);

        }
    }
}