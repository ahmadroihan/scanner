package com.example.masfuadiiskandarsy.scannercleon2;

/**
 * Created by Masfuadi Iskandar Sy on 26/01/2017.
 */

public class Result {
    String kodeMitra;
    String namaMitra;
    String alamatMitra;
    String noHp;
    String jumlah;

    public String getJumlah() {
        return jumlah;
    }

    public String getKodeMitra() {
        return kodeMitra;
    }

    public String getNamaMitra() {
        return namaMitra;
    }

    public String getAlamatMitra() {
        return alamatMitra;
    }

    public String getNoHp() {
        return noHp;
    }
}
