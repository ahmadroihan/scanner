package com.example.masfuadiiskandarsy.scannercleon2;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class login extends Activity implements View.OnClickListener{
    private EditText username, password;
    private Button btnLogin;
    private Spinner spinLevel;
    private String url, success;
    SessionManager sessionManager;
    JSONParser jparser;
    static boolean a = false;
    String[] items = {"PILIH", "ADMIN", "PETUGAS"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sessionManager = new SessionManager(getApplicationContext());
        username = (EditText)findViewById(R.id.idUsername);
        password = (EditText)findViewById(R.id.idPassword);
        spinLevel = (Spinner)findViewById(R.id.SpinLevel);
        btnLogin = (Button)findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);

        ArrayAdapter<String> aa = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_checked, items);
        aa.setDropDownViewResource(android.R.layout.simple_list_item_checked);
        spinLevel.setAdapter(aa);

    }
    public class masuk extends AsyncTask<String, String, JSONObject>{
        ArrayList<HashMap<String, String>> contctList = new ArrayList<HashMap<String, String>>();
        ProgressDialog pDialog;
        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            pDialog = new ProgressDialog(login.this);
            pDialog.setMessage("Mohon Tunggu...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }
        @Override
        protected JSONObject doInBackground(String...args){
            JSONParser jparser = new JSONParser();
            JSONObject json = jparser.getJSONFromUrl(url);
            if (json==null){
                a=false;
            }
            else
                a=true;
            return json;
        }
        @Override
        protected void onPostExecute(JSONObject json){
            if (a==true){
                try{
                    success = json.getString("success");
                    Log.e("error", "nilai sukses"+success);
                    JSONArray hasil = json.getJSONArray("login");
                    if (success.equals("1")){
                        for (int i = 0; i<hasil.length();i++){
                            JSONObject c = hasil.getJSONObject(i);
                            String kodeAdmin = c.getString("kodeAdmin").trim();
                            String namaAdmin = c.getString("namaAdmin").trim();
                            String username = c.getString("username").trim();
                            String levelAdmin = c.getString("levelAdmin").trim();
                            sessionManager.createLoginSession(kodeAdmin, namaAdmin, username, levelAdmin);
                            startActivity(new Intent(login.this, MainActivity.class));
                            finish();
                        }
                    }else {
                        Log.e("error", "tidak bisa ambil data 0");
                    }
                }catch (Exception e){
                    pDialog.dismiss();
                    Toast.makeText(login.this, "id dan password yg anda masukkan salah", Toast.LENGTH_SHORT).show();
                    username.setText("");
                    password.setText("");
                    Log.e("error", "tidak bisa ambil dataa 1");
                }
            }
            else {
                pDialog.dismiss();
                Toast.makeText(login.this, "jaringan bermsalah, silahkan cek jaringan anda", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void proses(){
        koneksi k = new koneksi();
        String isi = k.isi_koneksi();
        url = isi+"login.php?"+"username="+username.getText().toString()+"&password="+password.getText().toString()+"&levelAdmin="+spinLevel.getSelectedItem().toString();
        if (username.getText().toString().trim().length()>0
                &&password.getText().toString().trim().length()>0){
            new masuk().execute();
        }
        else {
            Toast.makeText(login.this, "usename dan password masih kosong", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        if (v==btnLogin){
            proses();
        }
    }
}
