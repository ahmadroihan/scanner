package com.example.masfuadiiskandarsy.scannercleon2;

/**
 * Created by Masfuadi Iskandar Sy on 13/01/2017.
 */
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

@SuppressLint("CommitPrefEdits")
public class SessionManager {

    SharedPreferences pref;
    Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "Sesi";

    private static final String IS_LOGIN = "IsLoggedIn";
    public static final String KEY_NAME = "namaAdmin";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_ID = "kodeAdmin";
    public static final String KEY_LEV = "levelAdmin";

    public SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createLoginSession(String namaAdmin, String username, String kodeAdmin, String levelAdmin){
        editor.putBoolean(IS_LOGIN, true);

        editor.putString(KEY_NAME, namaAdmin);
        editor.putString(KEY_USERNAME, username);
        editor.putString(KEY_ID, kodeAdmin);
        editor.putString(KEY_LEV, levelAdmin);
        editor.commit();
    }

    public void checkLogin(){
        if(!this.isLoggedIn()){
            Intent i = new Intent(_context, login.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);

        }
    }

    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();

        user.put(KEY_NAME, pref.getString(KEY_NAME, null));
        user.put(KEY_USERNAME, pref.getString(KEY_USERNAME, null));
        user.put(KEY_ID, pref.getString(KEY_ID, null));
        user.put(KEY_LEV, pref.getString(KEY_LEV, null));
        return user;
    }
    public void logoutUser(){
        editor.clear();
        editor.commit();
        Intent i = new Intent(_context, Splash_screen.class);//backup e iki asline login.class;
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(i);
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }
}