package com.example.masfuadiiskandarsy.scannercleon2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
/**
 * Created by Masfuadi Iskandar Sy on 11/01/2017.
 */

public class tab2 extends Fragment{

    private String urll;
    private List<Result> results = new ArrayList<>();
    private RecylerAdapteReport viewAdapter;

    public static final String KODE_MR = "kodeMitra";
    public static final String NAMA_MR = "namaMitra";
    public static final String JUMLAH_MR = "jumlah";

    @BindView(R.id.recyclerViewReport) RecyclerView recyclerView;
    @BindView(R.id.progress_barReport) ProgressBar progressBar;
    @BindView(R.id.tab2Gagal)TextView tab2Gagal;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.tab2,container,false);

        ButterKnife.bind(this, v);

        koneksi k = new koneksi();
        urll = k.isi_koneksi();

        viewAdapter = new RecylerAdapteReport(getActivity(), results);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(viewAdapter);

        loadDatareport();

        final SwipeRefreshLayout dorefresh = (SwipeRefreshLayout)v.findViewById(R.id.swipeRefreshReport);
        dorefresh.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        dorefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItem();
            }
            void refreshItem(){
                loadDatareport();
                onItemLoad();
            }
            void onItemLoad(){
                dorefresh.setRefreshing(false);
            }
        });

        return v;

        }
    private void loadDatareport() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(urll)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RegisterAPI api = retrofit.create(RegisterAPI.class);
        Call<Value> call = api.viewReport();
        call.enqueue(new Callback<Value>() {
            @Override
            public void onResponse(Call<Value> call, Response<Value> response) {
                String value = response.body().getValue();
                progressBar.setVisibility(View.GONE);
                if (value.equals("1")) {
                    results = response.body().getResult();
                    viewAdapter = new RecylerAdapteReport(getActivity(), results);
                    recyclerView.setAdapter(viewAdapter);
                }
            }
            @Override
            public void onFailure(Call<Value> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                tab2Gagal.setText("No Connection");
                Toast.makeText(getActivity(), "Jaringan Buruk", Toast.LENGTH_SHORT).show();
            }
        });
    }
}